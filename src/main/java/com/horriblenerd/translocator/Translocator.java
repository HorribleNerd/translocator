package com.horriblenerd.translocator;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.PistonEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static net.minecraft.util.Direction.DOWN;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(Translocator.MODID)
public class Translocator {

    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();
    public static final String MODID = "translocator";

    public Translocator() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void onPistonEvent(PistonEvent.Post event) {
        if (event.getPistonMoveType() == PistonEvent.PistonMoveType.RETRACT) {
            IWorld world = event.getWorld();
            Direction direction = event.getDirection();
            BlockPos pos = event.getFaceOffsetPos();
            BlockPos newPos = pos.offset(direction.getOpposite(), 2);
            List<Entity> entityList = world.getLoadedEntitiesWithinAABB(Entity.class, new AxisAlignedBB(pos));
            // Also pull crouched/swimming players that get squished by the piston head
            if (direction == DOWN) {
                world.getLoadedEntitiesWithinAABB(PlayerEntity.class,
                        new AxisAlignedBB(pos.getX(), pos.getY() + 1, pos.getZ(), pos.getX() + 1, pos.getY() - 1, pos.getZ() + 1))
                        .stream()
                        .filter(Entity::isActualySwimming)
                        .forEach(entityList::add);
            }
            if (!entityList.isEmpty()) {
                entityList.forEach(e -> {
                    // Distance is needed to move the entity to the relative position instead of the origin of the block,
                    // and also to make sure players can escape an infinite loop of piston translocators.
                    double distanceX = Math.abs(pos.getX()) - Math.abs(e.getPosX());
                    double distanceZ = Math.abs(pos.getZ()) - Math.abs(e.getPosZ());
                    e.setPosition(newPos.getX() + distanceX, newPos.getY(), newPos.getZ() + distanceZ);
                });
            }
        }
    }

}
